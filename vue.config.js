const SitemapPlugin = require('sitemap-webpack-plugin').default;
const paths = [
  {
    path: '/',
    lastmod: '2021-06-13',
    priority: 0.9,
    changefreq: 'monthly'
  },
  {
    path: '/coming-soon',
    lastmod: '2021-06-13',
    priority: 0.6,
    changefreq: 'monthly'
  }
];
module.exports = {
  css: {
    loaderOptions: {
      sass: {
        additionalData: `@import "~@/assets/scss/variables.scss";`
      }
    }
  },
  pluginOptions: {
    prerenderSpa: {
      registry: undefined,
      renderRoutes: [
        '/',
        '/coming-soon/'
      ],
      useRenderEvent: true,
      headless: true,
      onlyProduction: true
    }
  },
  configureWebpack: (config) => {
    if (process.env.NODE_ENV === 'production') {
      config.plugins.push(
        new SitemapPlugin({
          base: 'https://liove-netchef.vercel.app/',
          paths,
          options: {
            filename: 'sitemap.xml'
          }
        })
      );
    }
  }
}