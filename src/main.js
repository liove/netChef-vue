import Vue from 'vue'
import { BootstrapVue } from 'bootstrap-vue'
import router from './router'
import App from './App.vue'
// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// import assets styles
require('@/assets/scss/main.scss')

Vue.config.productionTip = false
// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)

const root = new Vue({
  router,
  render: h => h(App),
  // mounted: () => document.dispatchEvent(new Event('custom-render-trigger'))
})
document.addEventListener('DOMContentLoaded', function () {
  root.$mount('#app')
  setTimeout(function () {
    document.dispatchEvent(new Event('x-app-rendered'))
  }, 3000)
})
